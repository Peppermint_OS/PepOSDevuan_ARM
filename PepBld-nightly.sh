#!/bin/bash
PATH="/sbin:/usr/sbin:/usr/local/sbin:$PATH"

# Set the working folder variable
uchinanchu="$(pwd)"


# This cleanup might be better served in the BldHelper*.sh script.
# Create the build folder, move into it removing stale mountpoints and files there.
[ -e fusato ] && [ ! -d fusato ] && rm -f fusato || [ ! -e fusato ] && mkdir fusato
cd fusato

# Within the build, can be layered mounts inside multiple chroots.
umount $(mount | grep "${PWD}/chroot" | tac | cut -f3 -d" ") 2>/dev/null
for i in ./* ./.build ; do [ $i = ./cache ] && continue || rm -rf $i ; done


# Set of the structure to be used for the ISO and Live system.
# See /usr/lib/live/build/config for a full list of examples.
# Up above is the manual description of what options I used so far.
lb config noauto \
	--clean \
	--color \
	--quiet \
	--archive-areas "main contrib non-free non-free-firmware" \
	--architectures arm64 \
	--apt-recommends true \
	--binary-images iso-hybrid \
	--bootstrap-qemu-arch arm64 \
	--bootloaders grub-efi \
	--bootstrap-qemu-static /usr/sbin/qemu-debootstrap \
	--cache true \
	--mode debian \
	--distribution daedalus \
	--debootstrap-options "--merged-usr" \
	--initsystem sysvinit \
	--mirror-bootstrap http://deb.devuan.org/merged \
	--parent-mirror-bootstrap http://deb.devuan.org/merged \
	--parent-mirror-chroot http://deb.devuan.org/merged \
	--parent-mirror-chroot-security http://deb.devuan.org/merged \
	--parent-mirror-binary http://deb.devuan.org/merged \
	--parent-mirror-binary-security http://deb.devuan.org/merged \
	--mirror-chroot http://deb.devuan.org/merged \
	--mirror-chroot-security http://deb.devuan.org/merged \
	--firmware-binary false \
	--firmware-chroot false \
	--uefi-secure-boot enable \
	--iso-application "PeppermintOS" \
	--iso-preparer "PeppermintOS-https://peppermintos.com/" \
	--iso-publisher "Peppermint OS Team" \
	--iso-volume "PeppermintOS" \
	--image-name "PepOS" \
	--linux-flavours arm64 \
	--security false \
	--updates false \
	--backports false \
	--win32-loader false \
	--checksums sha512 \
	--zsync false \
     "${@}"

# Install the XFCE Desktop 
mkdir -p     $uchinanchu/fusato/config/package-lists/
echo xfce4 > $uchinanchu/fusato/config/package-lists/desktop.list.chroot 

# Install software
echo "# Install software to the squashfs for calamares to unpack to the OS.
marwaita-gtk-theme
tela-icon-theme
marwaita-for-xfwm
marwaita-peppermint-gtk-theme
pepermint-wallpapers
lightdm
lightdm-gtk-greeter
lightdm-gtk-greeter-settings
alsa-utils 
arandr 
arc-theme 
bluez 
bluez-firmware 
calamares-settings-debian 
calamares 
cups 
curl 
dconf-editor 
dkms 
dbus-x11 
efibootmgr 
firmware-linux 
firmware-qcom-soc
firmware-linux-nonfree 
firmware-misc-nonfree 
firmware-realtek 
firmware-atheros 
firmware-bnx2 
firmware-bnx2x 
firmware-brcm80211 
#firmware-intelwimax 
firmware-iwlwifi 
firmware-libertas 
firmware-netxen 
firmware-zd1211 
firmware-ralink 
raspi-firmware
fonts-cantarell 
fonts-liberation 
gdebi 
gir1.2-webkit2-4.0 
git 
gparted 
gnome-disk-utility 
gnome-system-tools 
#grub-pc 
gvfs-backends 
inputattach 
inxi 
locales
live-build 
menulibre
nala 
neofetch 
network-manager-gnome 
ntp 
os-prober 
pulseaudio-module-bluetooth 
python3-pip 
python3-tk 
python3-bs4 
python3-requests 
python3-ttkthemes 
python3-pyqt5 
python3-pyqt5.qtsvg 
python3-pyqt5.qtwebkit 
python3-pyqt5.qtwebengine 
python3-pil.imagetk
python3-apt 
screenfetch 
simple-scan 
smartmontools 
smbclient 
sqlite3 
synaptic 
system-config-printer 
mousepad 
xfce4-battery-plugin 
xfce4-clipman-plugin 
xfce4-power-manager 
xfce4-taskmanager 
xfce4-terminal 
xfce4-screenshooter 
xfce4-whiskermenu-plugin 
yad 
wireless-tools 
wget
f2fs-tools
xfsprogs
plymouth

" > $uchinanchu/fusato/config/package-lists/packages.list.chroot 


# Packages to be stored in /pool but not installed in the OS .
echo "# These packages are available to the installer, for offline use. 
efibootmgr
grub-common
grub2-common
grub-efi
grub-efi-arm64
grub-efi-arm64-bin
grub-efi-arm64-signed
libefiboot1
libefivar1
mokutil
isolinux
live-build
os-prober
shim-helpers-arm64-signed
shim-signed
shim-signed-common
shim-unsigned
" > $uchinanchu/fusato/config/package-lists/installer.list.binary 


# Setup the chroot structure
mkdir -p $uchinanchu/fusato/config/includes.binary
mkdir -p $uchinanchu/fusato/config/includes.bootstrap/etc/apt
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/local/bin
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/applications
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/icons/default
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/peppermint
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/pixmaps
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/xfce4/helpers
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/polkit-1/actions
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/fonts/pepconf
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/bin
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/sbin
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/lib/live/config
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/apt
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/apt/preferences.d
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/calamares
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/default
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/lightdm
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/live/config.conf.d
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/skel/.config/autostart
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/skel/.local/share
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/skel/Desktop
mkdir -p $uchinanchu/fusato/config/includes.chroot/etc/xdg/xfce4
mkdir -p $uchinanchu/fusato/config/includes.chroot/opt
mkdir -p $uchinanchu/fusato/config/includes.chroot/opt/pypep
mkdir -p $uchinanchu/fusato/config/includes.chroot/opt/pypep/dbpep
mkdir -p $uchinanchu/fusato/config/includes.chroot/opt/startpep
mkdir -p $uchinanchu/fusato/config/includes.chroot/opt/pepconf
mkdir -p $uchinanchu/fusato/config/includes.chroot/boot/grub
mkdir -p $uchinanchu/fusato/config/archives
mkdir -p $uchinanchu/fusato/config/hooks/live
mkdir -p $uchinanchu/fusato/config/hooks/normal
mkdir -p $uchinanchu/fusato/config/packages.chroot


# Copy single files to the chroot
cp $uchinanchu/pepaliases/bash_aliases  $uchinanchu/fusato/config/includes.chroot/etc/skel/.bash_aliases
cp $uchinanchu/pepcal/adddesktopicon/add-calamares-desktop-icon $uchinanchu/fusato/config/includes.chroot/usr/bin
cp $uchinanchu/pepcal/calamares/netinstall-* $uchinanchu/fusato/config/includes.chroot/etc/calamares
cp $uchinanchu/pepcal/calamares/settings.conf $uchinanchu/fusato/config/includes.chroot/etc/calamares
cp $uchinanchu/pepcal/bootloader-config $uchinanchu/fusato/config/includes.chroot/usr/sbin
cp $uchinanchu/peplightdm/* $uchinanchu/fusato/config/includes.chroot/etc/lightdm
cp $uchinanchu/pepsources/sources.list $uchinanchu/fusato/config/includes.chroot/opt/pepconf
cp $uchinanchu/PepProTools/xDaily $uchinanchu/fusato/config/includes.chroot/usr/local/bin
cp $uchinanchu/PepProTools/Welcome_auto.desktop $uchinanchu/fusato/config/includes.chroot/etc/skel/.config/autostart

# Copy directory contents to the chroot
cp $uchinanchu/pepapplication/*  $uchinanchu/fusato/config/includes.chroot/usr/share/applications
cp $uchinanchu/pepgrub/grub $uchinanchu/fusato/config/includes.chroot/etc/default
cp $uchinanchu/pephooks/live/* $uchinanchu/fusato/config/includes.chroot/usr/lib/live/config
cp $uchinanchu/pephooks/normal/* $uchinanchu/fusato/config/hooks/normal
cp $uchinanchu/pepissue/* $uchinanchu/fusato/config/includes.bootstrap/etc
cp $uchinanchu/pepissue/* $uchinanchu/fusato/config/includes.chroot/etc
cp $uchinanchu/pepissue/* $uchinanchu/fusato/config/includes.chroot/opt/pepconf
cp $uchinanchu/peposrelease/* $uchinanchu/fusato/config/includes.chroot/opt/pepconf
cp $uchinanchu/peposrelease/* $uchinanchu/fusato/config/includes.chroot/usr/lib
cp $uchinanchu/peppolkit/* $uchinanchu/fusato/config/includes.chroot/usr/share/polkit-1/actions
cp $uchinanchu/pepdb/* $uchinanchu/fusato/config/includes.chroot/opt/pypep/dbpep
cp $uchinanchu/pepuserconfig/* $uchinanchu/fusato/config/includes.chroot/etc/live/config.conf.d
cp $uchinanchu/PepProPixMaps/* $uchinanchu/fusato/config/includes.chroot/usr/share/pixmaps

# Copy recursive files and sub-directories, containing symlinks.
cp -r $uchinanchu/pepcal/calamares/branding $uchinanchu/fusato/config/includes.chroot/etc/calamares
cp -r $uchinanchu/pepcal/calamares/modules $uchinanchu/fusato/config/includes.chroot/etc/calamares
cp -r $uchinanchu/pepxfce/xfce4 $uchinanchu/fusato/config/includes.chroot/etc/skel/.config
cp -r $uchinanchu/pepgrub/themes $uchinanchu/fusato/config/includes.chroot/boot/grub
cp -r $uchinanchu/PepProTools/* $uchinanchu/fusato/config/includes.chroot/opt/pypep
cp -r $uchinanchu/peploadersplash/boot  $uchinanchu/fusato/config/includes.binary
cp -r $uchinanchu/peploadersplash/isolinux  $uchinanchu/fusato/config/includes.binary


# Resolves Synaptics issue. Might be better in a conf hook.
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/distro-info
mkdir -p $uchinanchu/fusato/config/includes.chroot/usr/share/python-apt/templates
cp -r $uchinanchu/pepinfo/devuan/* $uchinanchu/fusato/config/includes.chroot/usr/share/python-apt/templates
ln -s Devuan.info $uchinanchu/fusato/config/includes.chroot/usr/share/python-apt/templates/Peppermint.info
ln -s Devuan.mirrors $uchinanchu/fusato/config/includes.chroot/usr/share/python-apt/templates/Peppermint.mirrors
ln -s devuan.csv $uchinanchu/fusato/config/includes.chroot/usr/share/distro-info/peppermint.csv

# Place files unique to Nightly builds here.
cp    $uchinanchu/pepnightly/pepaliases/bash_aliases $uchinanchu/fusato/config/includes.chroot/etc/skel/.bash_aliases
cp    $uchinanchu/pepnightly/pepcal/install-peppermint $uchinanchu/fusato/config/includes.chroot/usr/bin
cp    $uchinanchu/pepnightly/pepcal/sources-final $uchinanchu/fusato/config/includes.chroot/usr/sbin
cp    $uchinanchu/pepnightly/pepcal/bootloader-config $uchinanchu/fusato/config/includes.chroot/usr/sbin
cp    $uchinanchu/pepnightly/pepmultimedia/* $uchinanchu/fusato/config/archives
cp -r $uchinanchu/pepnightly/peploadersplash/boot  $uchinanchu/fusato/config/includes.binary
cp -r $uchinanchu/pepnightly/peploadersplash/isolinux  $uchinanchu/fusato/config/includes.binary

# Build the ISO #
lb build noauto  #--debug --verbose 

