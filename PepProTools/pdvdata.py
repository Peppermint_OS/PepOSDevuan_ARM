"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* This script is used is used to show devuan updates
"""

import sys
from PyQt5.QtSql import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *


def initializeModel(model):
    """get thedisplay ready"""
    model.setTable('gitdata')
    model.setEditStrategy(QSqlTableModel.OnFieldChange)
    model.select()
    model.setHeaderData(0, Qt.Horizontal, "Date Updated")
    model.setHeaderData(1, Qt.Horizontal, "Comment")
    model.setHeaderData(2, Qt.Horizontal, "Change ID")


def createView(title, model):
    """Create the view"""
    view = QTableView()
    view.setModel(model)
    view.setWindowTitle(title)
    return view


def addrow():
    """Add the data"""
    print(model.rowCount())
    ret = model.insertRows(model.rowCount(), 1)
    print(ret)


def findrow(i):
    """ Find the data needed"""
    delrow = i.row()


if __name__ == '__main__':
    """ The main class to present everything"""
    app = QApplication(sys.argv)
    db = QSqlDatabase.addDatabase('QSQLITE')
    db.setDatabaseName('/opt/pypep/dbpep/welval.db')
    model = QSqlTableModel()
    delrow = -1
    initializeModel(model)

    view1 = createView("Table Model (View 1)", model)
    view1.clicked.connect(findrow)

    dlg = QDialog()
    layout = QVBoxLayout()
    layout.addWidget(view1)

    dlg.setLayout(layout)
    dlg.setGeometry(QRect(0, 0, 500, 700))
    dlg.setWindowTitle("Update History - Peppermint Devuan")
    dlg.setWindowIcon(QIcon('/usr/share/pixmaps/peppermint-old.png'))
    dlg.setWindowFlag(Qt.WindowMinimizeButtonHint, True)
    dlg.setWindowFlag(Qt.WindowMaximizeButtonHint, True)

    dlg.show()
    sys.exit(app.exec_())
