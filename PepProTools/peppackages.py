"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* this script is for the selected packages screen
*
"""
import subprocess
import tkinter as tk
from tendo import singleton
import ttkbootstrap as ttk
import putilities


# makes sure only a single instance is running
me = singleton.SingleInstance()

# list of all checkboxes and their current value
checkboxstates = []

# this will set the window name and  style.
instw = ttk.Window(themename="darkly")
# the window size
instw.geometry('680x520')
# the title of the window
instw.title("Suggested Packages")
# set the icon for the  window
instw.tk.call('wm', 'iconphoto', instw._w, tk.PhotoImage(
    file='/usr/share/pixmaps/peppermint-old.png'))

# ROW for all the items but the logo and the title
ROW = 20
ROW2 = 360

# main title style
spmt = ttk.Style()
spmt.configure(
    "F62817.TLabel",
    foreground="#ffffff",
    background="#222222",
    font=(
        "Helvetica",
        '22',
        'bold'))

# This is all the lables and verbiage used and thier placements
lblpmtitle = ttk.Label(
    instw,
    style="F62817.TLabel",
    text="Select Your Software Packages").place(
        x=140,
    y=10)
lblpmsubtitle = ttk.Label(
    instw,
    bootstyle="light",
    text="Darkened items are already installed. \nPackages are from Stable Repositories.").place(
        x=ROW2,
    y=310)
lblpmsubtitle = ttk.Label(
    instw,
    bootstyle="light",
    text="At PeppermintOS, we let our users decide what software is on their system.").place(
        x=140,
    y=40)

lblsoftware = ttk.Label(
    instw,
    bootstyle="light",
    text="Software").place(
        x=100,
    y=80)
lblbROWsers = ttk.Label(
    instw,
    bootstyle="light",
    text="Web Browsers").place(
        x=440,
    y=80)

# output textbox
textbox = ttk.Text(
    instw,
     background="#2f2f2f",
     foreground="white",
    state='disabled')
textbox.place(x=0, y=420, height=100, width=680)
textbox.configure(font=("GNU Unifont", '8'))


def install_check(appname, checkbox):
    """if this package is already installed, grey out the item"""
    if putilities.install_check(appname):
        checkbox.configure(state=["disabled"])


# firefox
firefox = tk.StringVar()
firefox.set("off")
cbfirefox = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=firefox,
    onvalue='firefox-esr',
    offvalue="off",
    text="Firefox: Mozilla's Open-source Browser")
cbfirefox.place(x=ROW2, y=100)
checkboxstates.append(firefox)
install_check("firefox-esr", cbfirefox)

# konqueror
konqueror = tk.StringVar()
konqueror.set("off")
cbkonqueror = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=konqueror,
    onvalue="konqueror",
    offvalue="off",
    text="Konqueror: KDE Browser and file manager")
cbkonqueror.place(x=ROW2, y=120)
checkboxstates.append(konqueror)
install_check("konqueror", cbkonqueror)

# epiphany-bROWser
epiphany = tk.StringVar()
epiphany.set("off")
cbepiphany = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=epiphany,
    onvalue="epiphany-browser",
    offvalue="off",
    text="Gnome Web (Epiphany): Intuitive Browser")
cbepiphany.place(x=ROW2, y=140)
checkboxstates.append(epiphany)
install_check("epiphany-bROWser", cbepiphany)

# falkon
falkon = tk.StringVar()
falkon.set("off")
cbfalkon = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=falkon,
    onvalue="falkon",
    offvalue="off",
    text="Falkon: Qt5 Browser by KDE")
cbfalkon.place(x=ROW2, y=160)
checkboxstates.append(falkon)
install_check("falkon", cbfalkon)

# torbROWser-launcher
torbROWser = tk.StringVar()
torbROWser.set("off")
cbtorbrowser = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=torbROWser,
    onvalue="torbrowser-launcher",
    offvalue="off",
    text="Tor Browser: Privacy Browser")
cbtorbrowser.place(x=ROW2, y=180)
checkboxstates.append(torbROWser)
install_check("torbrowser-launcher", cbtorbrowser)

# midori
midori = tk.StringVar()
midori.set("off")
cbmidori = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=midori,
    onvalue="midori",
    offvalue="off",
    text="Midori: Fast, lightweight Browser")
cbmidori.place(x=ROW2, y=200)
checkboxstates.append(midori)
install_check("midori", cbmidori)

# qutebROWser
qutebROWser = tk.StringVar()
qutebROWser.set("off")
cbqutebROWser = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=qutebROWser,
    onvalue="qutebrowser",
    offvalue="off",
    text="Qutebrowser: Vim-like Browser")
cbqutebROWser.place(x=ROW2, y=220)
checkboxstates.append(qutebROWser)
install_check("qutebrowser", cbqutebROWser)

# luakit
luakit = tk.StringVar()
luakit.set("off")
cbluakit = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=luakit,
    onvalue="luakit",
    offvalue="off",
    text="Luakit: Browser extensible by Lua")
cbluakit.place(x=ROW2, y=240)
checkboxstates.append(luakit)
install_check("luakit", cbluakit)

# chromium
chromium = tk.StringVar()
chromium.set("off")
cbchromium = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=chromium,
    onvalue="chromium",
    offvalue="off",
    text="Chromium: Google's Open-Source Browser")
cbchromium.place(x=ROW2, y=260)
checkboxstates.append(chromium)
install_check("chromium", cbchromium)

# atril
atril = tk.StringVar()
atril.set("off")
cbatril = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=atril,
    onvalue="atril",
    offvalue="off",
    text="Atril: a multi-page document viewer")
cbatril.place(x=ROW, y=100)
checkboxstates.append(atril)
install_check("atril", cbatril)

# transmission
transmission = tk.StringVar()
transmission.set("off")
cbtransmission = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=transmission,
    onvalue="transmission",
    offvalue="off",
    text="Transmission BitTorrent Client")
cbtransmission.place(x=ROW, y=120)
checkboxstates.append(transmission)
install_check("transmission", cbtransmission)

# ARandR
arandr = tk.StringVar()
arandr.set("off")
cbarandr = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=arandr,
    onvalue="arandr",
    offvalue="off",
    text="ARandR: Another XRandR GUI")
cbarandr.place(x=ROW, y=140)
checkboxstates.append(arandr)
install_check("arandr", cbarandr)

# pmount
pmount = tk.StringVar()
pmount.set("off")
cbpmount = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=pmount,
    onvalue="pmount",
    offvalue="off",
    text="pmount: Standard mount program")
cbpmount.place(x=ROW, y=160)
checkboxstates.append(pmount)
install_check("pmount", cbpmount)

# dconf-editor
dconfeditor = tk.StringVar()
dconfeditor.set("off")
cbdconfeditor = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=dconfeditor,
    onvalue="dconf-editor",
    offvalue="off",
    text="DConf Editor: For environment settings")
cbdconfeditor.place(x=ROW, y=180)
checkboxstates.append(dconfeditor)
install_check("dconf-editor", cbdconfeditor)

# gpicview
gpicview = tk.StringVar()
gpicview.set("off")
cbgpicview = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=gpicview,
    onvalue="gpicview",
    offvalue="off",
    text="GpicView image viewer")
cbgpicview.place(x=ROW, y=200)
checkboxstates.append(gpicview)
install_check("gpicview", cbgpicview)

# parole
parole = tk.StringVar()
parole.set("off")
cbparole = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=parole,
    onvalue="parole",
    offvalue="off",
    text="Parole: Media player for Xfce")
cbparole.place(x=ROW, y=220)
checkboxstates.append(parole)
install_check("parole", cbparole)

#GNU Firewall
gufw = tk.StringVar()
gufw.set("off")
cbgufw = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=gufw,
    onvalue="gufw",
    offvalue="off",
    text="gufw: GNU Firewall")
cbgufw.place(x=ROW, y=240)
checkboxstates.append(gufw)
install_check("gufw", cbgufw)

# snapd
snapd = tk.StringVar()
snapd.set("off")
cbsnapd = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=snapd,
    onvalue="snapd",
    offvalue="off",
    text="Snap Package Platform")
cbsnapd.place(x=ROW, y=260)
checkboxstates.append(snapd)
install_check("snapd", cbsnapd)

# flatpak
flatpak = tk.StringVar()
flatpak.set("off")
cbflatpak = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=flatpak,
    onvalue="flatpak",
    offvalue="off",
    text="Flatpak Packages Platform")
cbflatpak.place(x=ROW, y=280)
checkboxstates.append(flatpak)
install_check("flatpak", cbflatpak)

# gnome-software
gnomesoftware = tk.StringVar()
gnomesoftware.set("off")
cbgnomesoftware = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=gnomesoftware,
    onvalue="gnome-software",
    offvalue="off",
    text="Gnome software store")
cbgnomesoftware.place(x=ROW, y=300)
checkboxstates.append(gnomesoftware)
install_check("gnome-software", cbgnomesoftware)

# timshift
timeshift = tk.StringVar()
timeshift.set("off")
cbtimeshift = ttk.Checkbutton(
    instw,
    bootstyle="danger",
    variable=timeshift,
    onvalue="timeshift",
    offvalue="off",
    text="TimeShift: Backup Tool")
cbtimeshift.place(x=ROW, y=320)
checkboxstates.append(timeshift)
install_check("timeshift", cbtimeshift)


def install(applist):
    """ install the applications"""
    btnwk = ttk.Button(instw, text="Installing...", bootstyle="danger", state='disabled')
    btnwk.place(x=245, y=375, height=30, width=150)
    btn.place_forget()
    textbox.configure(state='normal')

    def textout(line):
        textbox.insert(tk.END, line)
        textbox.update_idletasks()
        textbox.see("end")
        print(line + "\n")

    if len(applist) > 0:
        with subprocess.Popen(["apt-get", "install", "-y"] + applist,
                                shell=False, stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                universal_newlines=True) as point:
            if point.stdout:
                for line in point.stdout:
                    textout(line)
            if point.stderr:
                for line in point.stderr:
                    textout(line)
    textout("Done.")
    textbox.configure(state='disabled')
    btnwk.place_forget()
    btn.place(x=245, y=375, height=30, width=150)
    


def fin():
    """Finish close window"""
    instw.destroy()


def getcheckboxes():
    """This will verify the check boxes"""
    appqueue = []
    for item in checkboxstates:
        if item.get() != "off":
            appqueue.append(item.get())
    print(appqueue)

    if len(appqueue) > 0:
        install(appqueue)
    else:
        textbox.configure(state='normal')
        textbox.insert(
            tk.END,
            "No packages selected. Please select a package.\n")
        textbox.update_idletasks()
        textbox.see("end")
        textbox.configure(state='disabled')


# inital Begin  install button
btn = ttk.Button(
    instw,
    text="Install Selected",
    bootstyle="light-outline",
    command=getcheckboxes)
##Placement of the Begin install button##
btn.place(x=245, y=375, height=30, width=150)
btnf = ttk.Button(
    instw,
    bootstyle="light-outline",
    text="Done",
    command=getcheckboxes)
btn.place(x=245, y=375, height=30, width=150)
##Start the window#
instw.mainloop()
